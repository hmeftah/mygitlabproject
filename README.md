# mygitlabproject

Personal gitlab project

### Pre-requis pour installer un virtualenv, Ansible et Docker sur Ubuntu 20.04
```shell
sudo apt update   # update all packages
sudo apt -y install sshpass # allow using ssh with a password
sudo apt -y install python3-venv

#fork and clone ---> git clone   https://gitlab.com/hmeftah/mygitlabproject.git
cd mygitlabproject
python3 -m venv venv # set up the module venv in the directory venv
source venv/bin/activate # activate the python virtualenv
pip3 install wheel # set for permissions purpose
pip3 install ansible # install ansible
pip3 install sqlalchemy # install access to a postgres database from python
pip3 install psycopg2-binary # driver for postgres 
pip3 install natsort # for sorting alphanum 
pip3 install requests # extra packages 
ansible --version  # check version number , should be the latest 2.12.1+
cp inventory_template inventory
ansible-playbook -i inventory install_docker_ubuntu.yml --limit local # run a playbook
# Fermer votre IDE et le demarrer a nouveau pour que les changements soient appliques
cd ansible-course 
source venv/bin/activate # activate the python virtualenv
docker ps 
```